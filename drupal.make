; This file was auto-generated by drush make
core = 7.x

api = 2
projects[drupal][version] = "7.21"

; Modules
projects[views_bulk_operations][version] = "3.1"

projects[admin_menu][version] = "3.0-rc4"

projects[backup_migrate][version] = "2.4"

projects[ctools][version] = "1.2"

projects[calendar][version] = "3.4"

projects[css_injector][version] = "1.8"

projects[date][version] = "2.6"

projects[devel][version] = "1.3"

projects[entity][version] = "1.0"

projects[features][version] = "2.0-beta1"

projects[media][version] = "1.3"

projects[js_injector][version] = "2.0"

projects[libraries][version] = "2.1"

projects[link][version] = "1.1"

projects[media_youtube][version] = "2.0-rc2"

projects[panels][version] = "3.3"

projects[sharethis][version] = "2.5"

projects[timeline][version] = "3.x-dev"

projects[views][version] = "3.5"

; Themes
projects[zen][version] = "5.1"

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[simile_timeline][download][type] = "git"
libraries[simile_timeline][download][url] = "https://github.com/xamanu/SIMILE-Timeline-libraries.git"
libraries[simile_timeline][directory_name] = "simile_timeline"
libraries[simile_timeline][type] = "library"

